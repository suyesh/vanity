class AddFieldsToGig < ActiveRecord::Migration
  def change
    add_column :gigs, :locations, :text
    add_column :gigs, :additional_info, :text
  end
end
