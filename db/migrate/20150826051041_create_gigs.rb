class CreateGigs < ActiveRecord::Migration
  def change
    create_table :gigs do |t|
      t.string :gig_type
      t.string :role
      t.string :title
      t.text :description
      t.decimal :offer
      t.string :offer_type
      t.datetime :from
      t.datetime :to

      t.timestamps null: false
    end
  end
end
