Rails.application.routes.draw do
  get 'page/home'

  get 'page/about'

  get 'page/faq'

  root "gigs#new"
  resources :gigs
end
