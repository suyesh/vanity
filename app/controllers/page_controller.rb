class PageController < ApplicationController
  def home
    render layout: "pages"
  end

  def about
  end

  def faq
  end
end
