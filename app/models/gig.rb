class Gig < ActiveRecord::Base
  GIG_TYPE = %w(Hair Makeup Wardrobe Hair&Makeup)
  GIG_ROLE = %w(Assistant Head)
  OFFER_TYPE = %w(/hour /day /week)
end
